import Vue from "vue";
import App from "./App.vue";
import FontAwesomeIcon from "./assets/font-awesome";
import "./registerServiceWorker";
import api from "./config/api";
import vmodal from "vue-js-modal";

Vue.use(vmodal, { dynamic: true, dynamicDefaults: { clickToClose: false } });
Vue.config.productionTip = false;
Vue.prototype.$http = api;

Vue.component("fa-icon", FontAwesomeIcon);

new Vue({
  render: h => h(App)
}).$mount("#app");
