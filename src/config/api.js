/* eslint-disable no-param-reassign */
import axios from 'axios';

const baseURL = 'http://api.institutoholistico.com.br';

const api = axios.create({
  baseURL,
  headers: {
    Accept: 'application/json',
    'Content-type': 'multipart/form-data',
  },
});

export default api;
