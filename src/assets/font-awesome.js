import { library } from '@fortawesome/fontawesome-svg-core';
import { faAngleRight, faAngleLeft, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faAngleRight);
library.add(faSpinner);
library.add(faAngleLeft);
library.add(faFacebookF);
library.add(faInstagram);

export default FontAwesomeIcon;
